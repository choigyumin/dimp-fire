import os
import glob
import shutil

import numpy as np
import imageio
import cv2

import fire

__all__ = [
    'copy',
    'square_clip',
    'concat',
    'crop',
    'make_video']

class Dimp(object):
    """Deepstudio Image Processor
    
    Currently, functions are updating...
    $ python dimp.py -h
    for more info.
    """
    def __init__(self, input_dir, output_dir):
        self.input_dir = input_dir
        self.output_dir = output_dir

    def mkdir(self, dir_path):
        if not os.path.exists(dir_path):
            os.mkdir(dir_path)

    def sg(self, path):
        return sorted(glob.glob(path))


    def copy(self):
        input_dir = self.input_dir
        output_dir = self.output_dir
        input_list = self.sg(os.path.join(input_dir, "*"))
        self.mkdir(output_dir)

        length = len(input_list)
        for i, path in enumerate(input_list):
            output_img_path = os.path.join(output_dir, '%04d.jpg' % i)
            shutil.copy(path, output_img_path)

            print(f'{i}/{length} : {output_img_path}', end='\r', flush=True)


    def square_clip(self, size, resize=None, channel=3):
        input_dir = self.input_dir
        output_dir = self.output_dir
        input_list = self.sg(os.path.join(input_dir, "*"))
        self.mkdir(output_dir)

        h, w = imageio.imread(input_list[0]).shape[:2]

        img_size = max(h, w)

        assert img_size >= size, 'size must be smaller than max(h, w)'

        length = len(input_list)
        for i, path in enumerate(input_list):
            back = np.zeros((img_size, img_size, channel), dtype=np.uint8)
            img = imageio.imread(path)[:,:,:channel]

            x_off = (img_size - w) // 2
            y_off = (img_size - h) // 2

            back[y_off:y_off+h, x_off:x_off+w] = img

            x_off = (img_size - size) // 2
            y_off = (img_size - size) // 2

            back = back[y_off:y_off+size, x_off:x_off+size]

            if resize:
                back = cv2.resize(back, (resize, resize))

            output_img_path = os.path.join(output_dir, '%04d.jpg' % i)
            imageio.imwrite(output_img_path, back)

            print(f'{i+1}/{length} : {output_img_path}', end='\r', flush=True)


    def concat(self, channel=3, make_video=False):
        input_dir = self.input_dir
        output_dir = self.output_dir
        input_list = self.sg(os.path.join(input_dir, "*"))

        assert len(set([len(paths) for paths in input_list])) <= 1, \
            'length of paths must be same.'

        self.mkdir(output_dir)

        length = len(input_list[0])

        for i, path_pair in enumerate(zip(*input_list)):
            img = np.concatenate([imageio.imread(path)[:,:,:channel] for path in path_pair], axis=1)
            output_img_path = os.path.join(output_dir, '%04d.jpg'%i)

            imageio.imwrite(output_img_path, img)
            print(f'{i+1}/{length} : {output_img_path}', end='\r', flush=True)


    def crop(self, bbox_loc, resize=None, channel=3):
        input_dir = self.input_dir
        output_dir = self.output_dir
        input_list = self.sg(os.path.join(input_dir, "*"))
        self.mkdir(output_dir)
        h, w = imageio.imread(input_list[0]).shape[:2]

        y, x, bw, bh = bbox_loc
        length = len(input_list)
        for i, path in enumerate(input_list):
            back = np.zeros((h, w, channel), dtype=np.uint8)
            img = imageio.imread(path)[:, :, :channel]

            back = img[y:y + bh, x:x + bw, :]

            if resize:
                back = cv2.resize(back, resize, interpolation = cv2.INTER_AREA)

            output_img_path = os.path.join(output_dir, '%04d.jpg' % i)
            imageio.imwrite(output_img_path, back)

            print(f'{i + 1}/{length} : {output_img_path}', end='\r', flush=True)


    def make_video(self):
        input_dir = self.input_dir
        output_dir = self.output_dir
        input_list = self.sg(os.path.join(input_dir, "*"))
        
        video = None
        for i, path in enumerate(input_list):
            print(os.path.basename(path), end='\r', flush=True)

            img = cv2.imread(path)

            if video is None:
                h, w = img.shape[:2]
                video = cv2.VideoWriter(output_path, cv2.VideoWriter_fourcc(*"MJPG"), 30, (w, h))

            video.write(img)

        cv2.destroyAllWindows()
        video.release()

if __name__ == "__main__":
    fire.Fire(Dimp)
    
